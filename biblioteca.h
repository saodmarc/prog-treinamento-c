
#include <stdlib.h>

//Biblioteca digital - operações:
// - registrar livro
// - consultar livro por id
// - exibir todos os livros

/**
 * Representa um livro a ser registrado na biblioteca digital.
 */
struct LIVRO {
    unsigned long int id;   //< Identificador único do livro registrado
    char nome[80];          //< Nome do livro
    char nome_autor[500];   //< Nome do(s) autor(es) do livro
    unsigned short int ano; //< Ano de lançamento
};

/**
 * Biblioteca digital 2.0, contendo uma lista de livros atualmente cadastrados.
 */
struct BIBLIOTECA {
    unsigned long int total_livros;     //< Total de livros atualmente cadastrados na biblioteca.

    struct LIVRO* livros;               //< Ponteiro para array de livros
};


/**
 * Representa possíveis resultados de operações na biblioteca.
 */
enum BIBLIOTECA_RESULTADO_ENUM {
    BIBLIOTECA_ERRO_OPERACAO_INVALIDA = -3,         //< Enumeração para erro genérico
    BIBLIOTECA_ERRO_PARAMETROS_INVALIDOS = -2,      //< Enumeração para erro ao registrar um livro
    BIBLIOTECA_ERRO_NAO_TEM_LIVROS = -1,            //< Enumeração para erro ao registrar um livro
    BIBLIOTECA_ERRO_LIVRO_NAO_ENCONTRADO = -4,      //< Enumeração para erro ao encontrar um livro
    BIBLIOTECA_SUCESSO = 0                          //< Enumeração para erro genérico
};

/**
 * Registra um livro na lista de livros da biblioteca digital.
 * 
 * @param[in] livro     - struct contendo informações do livro a ser registrado
 * 
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - livro registrado com sucesso
 *      BIBLIOTECA_ERRO_PARAMETROS_INVALIDOS    - informações invalidas na struct passada por parâmetro
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_RegistrarLivro(struct LIVRO* livro);



/**
 * Retorna ponteiro para um livro da biblioteca digital, com base no Id especificado como parâmetro de entrada.
 * Importante: Não é necessário desalocar a memória referenciada por este ponteiro.
 * 
 * @param[in] id                        - Id do livro a ser consultado
 * @param[out] livro_retornado          - Ponteiro da struct LIVRO retornada pela função.
 *                                        Se o livro não for encontrado, aqui é retornado NULL.
 *                                        OBS: não é necessário desalocar a memória referenciada por este ponteiro.
 * 
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - livro encontrado com sucesso
 *      BIBLIOTECA_ERRO_LIVRO_NAO_ENCONTRADO    - livro com Id especificado não encontrado
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_ConsultarLivroPorId(unsigned long int id, struct LIVRO* livro_retornado);


/**
 * Retorna ponteiro para biblioteca, contendo todos os livros cadastrados.
 * 
 * @param[out] biblioteca     - ponteiro para BIBLIOTECA
 * 
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - biblioteca retornada com sucesso
 *      BIBLIOTECA_ERRO_OPERACAO_INVALIDA       - erro inesperado na operação
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_RetornarBibliotecaDigital(struct BIBLIOTECA* biblioteca);