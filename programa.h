

#include <stdlib.h>
#include <stdio.h>

#include "biblioteca.h"

//Operações do programa "biblioteca digital"

int ExecutarPrograma();

void RegistrarLivro();

void ConsultarLivroPorId();

void ExibirTodosOsLivros();

void ExibirLivro(struct LIVRO* livro);