
#include "programa.h"

//Ponto de entrada do executável
int main(int argc, char** argv) {

    int status = ExecutarPrograma();

    return status;
}


int ExecutarPrograma() {

    //variáveis do programa
    int terminou = 0;
    int opcao = -1;

    while (terminou != 1) {

        printf("BIBLIOTECA DIGITAL 2.0\n");

        printf("1 - registrar livro\n");
        printf("2 - consultar livro por id\n");
        printf("3 - exibir todos os livros\n");
        printf("\n\n4 - SAIR DA BIBLIOTECA DIGITAL\n");

        printf("Digite uma das opcoes acima: ");
        scanf("%d%*c", &opcao);

        switch(opcao) {
            case 1:
                RegistrarLivro();
                break;
            case 2:
                ConsultarLivroPorId();
                break;
            case 3:
                ExibirTodosOsLivros();
                break;
            case 4:
                terminou = 1;
        }

    }

    return 0;
}

void RegistrarLivro() {

    struct LIVRO* novo_livro = (struct LIVRO*) malloc(sizeof(struct LIVRO));

    enum BIBLIOTECA_RESULTADO_ENUM resultado;

    //Entrada: Nome do livro
    printf("\nDigite o nome do livro (maximo de %ld letras):\n", sizeof(novo_livro->nome)/sizeof(char));
    fgets(novo_livro->nome, sizeof(novo_livro->nome)/sizeof(char), stdin);

    //Entrada: Nome do autor do livro
    printf("\nDigite o nome do autor:\n");
    fgets(novo_livro->nome_autor, sizeof(novo_livro->nome_autor) / sizeof(char), stdin);

    //Entrada: Ano do livro
    printf("\nDigite o ano do livro:\n");
    scanf("%hu", &novo_livro->ano);

    resultado = Biblioteca_RegistrarLivro(novo_livro);

    if (resultado == BIBLIOTECA_SUCESSO) {
        printf("\nLivro cadastrado com sucesso.\n");
    } else {
        printf("\nErro ao cadastrar um livro novo.\n");
    }

    printf("\n\n");

    free(novo_livro);
}

void ConsultarLivroPorId() {
    unsigned long int id;
    struct LIVRO livro_retornado;
    enum BIBLIOTECA_RESULTADO_ENUM resultado;

    printf("\nDigite o Id do livro:\n");
    scanf("%lu", &id);
    // printf("%ul", id);

    resultado = Biblioteca_ConsultarLivroPorId(id, &livro_retornado);

    if (resultado == BIBLIOTECA_SUCESSO) {
        ExibirLivro(&livro_retornado);
    }
    if (resultado == BIBLIOTECA_ERRO_LIVRO_NAO_ENCONTRADO) {
        printf("\nLivro nao encontrado.\n");
    }
}

void ExibirTodosOsLivros() {
    struct BIBLIOTECA biblioteca;
    enum BIBLIOTECA_RESULTADO_ENUM resultado;
    // printf(">>exibir todos os livros\n");

    resultado = Biblioteca_RetornarBibliotecaDigital(&biblioteca);

    if (resultado == BIBLIOTECA_SUCESSO) {
        //Itera sobre todos os livros da biblioteca para exibir suas informações
        for (int i = 0; i < biblioteca.total_livros; i++) {
            ExibirLivro(biblioteca.livros + i);
        }
    }

    //Tratamento de erros
    if (resultado == BIBLIOTECA_ERRO_OPERACAO_INVALIDA) {
        printf("\nErro ao exibir todos os livros da biblioteca.\n");
    }

}

void ExibirLivro(struct LIVRO* livro) {
    // printf(">>exibir livro\n");
    if (livro != NULL) {
        printf("\n\n>> Id %ld: ", livro->id);
        printf("%s", livro->nome);
        printf("Autor: %s", livro->nome_autor);
        printf("Ano: %d", livro->ano);
        printf("\n\n");
    }
}
