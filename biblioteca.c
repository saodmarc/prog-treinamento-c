
#include "biblioteca.h"


static struct BIBLIOTECA _biblioteca = { 0 };

/**
 * Registra um livro na lista de livros da biblioteca digital.
 *
 * @param[in] livro     - struct contendo informações do livro a ser registrado
 *
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - livro registrado com sucesso
 *      BIBLIOTECA_ERRO_PARAMETROS_INVALIDOS    - informações invalidas na struct passada por parâmetro
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_RegistrarLivro(struct LIVRO* livro) {

    enum BIBLIOTECA_RESULTADO_ENUM resultado = BIBLIOTECA_SUCESSO;

    if (livro == NULL){
        resultado = BIBLIOTECA_ERRO_PARAMETROS_INVALIDOS;
    } else {

        //Alocação de memória
        if (_biblioteca.total_livros == 0) {
            _biblioteca.livros = (struct LIVRO*) malloc(sizeof(struct LIVRO));
            _biblioteca.total_livros++;
        } else {
            _biblioteca.livros = realloc(_biblioteca.livros, sizeof(struct LIVRO) * (_biblioteca.total_livros + 1));
            _biblioteca.total_livros++;
        }

        //Preenche o conteúdo do último livro inserido
        _biblioteca.livros[_biblioteca.total_livros - 1] = *livro;

        //Campo id é auto-preenchido com o índice do livro
        _biblioteca.livros[_biblioteca.total_livros - 1].id = _biblioteca.total_livros - 1;

        resultado = BIBLIOTECA_SUCESSO;
    }

    return resultado;
}



/**
 * Retorna ponteiro para um livro da biblioteca digital, com base no Id especificado como parâmetro de entrada.
 * Importante: Não é necessário desalocar a memória referenciada por este ponteiro.
 *
 * @param[in] id                        - Id do livro a ser consultado
 * @param[out] livro_retornado          - Ponteiro da struct LIVRO retornada pela função.
 *                                        Se o livro não for encontrado, aqui é retornado NULL.
 *                                        OBS: não é necessário desalocar a memória referenciada por este ponteiro.
 *
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - livro encontrado com sucesso
 *      BIBLIOTECA_ERRO_LIVRO_NAO_ENCONTRADO    - livro com Id especificado não encontrado
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_ConsultarLivroPorId(unsigned long int id, struct LIVRO* livro_retornado) {

    enum BIBLIOTECA_RESULTADO_ENUM resultado = BIBLIOTECA_ERRO_LIVRO_NAO_ENCONTRADO;
    // livro_retornado = NULL;


    for (int i = 0;
         i < _biblioteca.total_livros || resultado != BIBLIOTECA_SUCESSO;
         i++) {
        //Busca livro por Id
        if (_biblioteca.livros[i].id == id) {
            *livro_retornado = _biblioteca.livros[i];
            resultado = BIBLIOTECA_SUCESSO;
        }
    }

    return resultado;
}


/**
 * Retorna ponteiro para biblioteca, contendo todos os livros cadastrados.
 *
 * @param[out] biblioteca     - ponteiro para BIBLIOTECA
 *
 * @returns BIBLIOTECA_RESULTADO_ENUM
 *      BIBLIOTECA_SUCESSO                      - biblioteca retornada com sucesso
 *      BIBLIOTECA_ERRO_OPERACAO_INVALIDA       - erro inesperado na operação
 */
enum BIBLIOTECA_RESULTADO_ENUM Biblioteca_RetornarBibliotecaDigital(struct BIBLIOTECA* biblioteca) {
    enum BIBLIOTECA_RESULTADO_ENUM resultado = BIBLIOTECA_ERRO_OPERACAO_INVALIDA;

    if (biblioteca != NULL) {
        *biblioteca = _biblioteca;
        resultado = BIBLIOTECA_SUCESSO;
    }

    return resultado;
}
