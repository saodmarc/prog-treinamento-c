

all: build

clean:
	rm *.obj *.gcda *.gcno *.ilk *.exe *.pdb

macro_definition:
APP=biblioteca_digital_2_0.exe

SRC = programa.c biblioteca.c

INC =
DEFINES =

FLAGS = -Wall
CC = gcc
OUT = /Fe$(APP)

FLAGS=
# CC = \MinGW\bin\gcc.exe
# COV = \MinGW\bin\gcov.exe
# OUT = -o sample_program.exe

build: clean compile

compile:
	@echo off
	@cls
	@echo Building Sample Program...

	$(CC) $(FLAGS) $(INC) $(DEFINES) $(SRC) $(OUT)

	del *.obj

run:
	@echo off
	@cls
	@call $(APP)
